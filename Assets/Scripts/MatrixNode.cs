﻿using System;

public class MatrixNode
{
    private double _realPathLength = 0;
    private double _heuristicLength = -1;

    public double CumulativeLength
    {
        get
        {
            return RealPathLength + HeuristicLength;
        }
    }
    private double RealPathLength
    {
        get
        {
            if (Parent != null)
                _realPathLength = Parent.RealPathLength + 1;

            return _realPathLength;
        }
    }
    private double HeuristicLength
    {
        get
        {
            if (_heuristicLength == -1) 
                _heuristicLength = Math.Sqrt((Destination.Position - Current.Position).sqrMagnitude);

            return _heuristicLength;
        }
    }

    public MatrixNode Parent { get; }
    public Hexagon Current { get; }
    private Hexagon Destination { get; }


    public MatrixNode(Hexagon current, Hexagon destination)
    {
        Current = current;
        Destination = destination;
    }

    public MatrixNode(Hexagon current, Hexagon destination, MatrixNode parent)
    {
        Parent = parent;
        Current = current;
        Destination = destination;     
    } 
}
