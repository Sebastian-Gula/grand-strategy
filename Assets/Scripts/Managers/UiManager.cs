﻿using UnityEngine;

public class UiManager : MonoBehaviour
{
    public GameObject BasicInfoPanel;

    public CityUiBasic CityUiBasic;
    public CityUi CityUi;

    public GameObject CastleBasicUi;
    public GameObject CastleUi;

    public GameObject VillageBasicUi;
    public GameObject VillageUi;

    static UiManager _instance;


    public static UiManager Instance
    {
        get
        {
            if (!_instance)
            {
                var gameObject = GameObject.FindGameObjectWithTag(Tags.UiManager);
                _instance = gameObject.GetComponent<UiManager>();
            }

            return _instance;
        }
    }

    public void ShowCityUiBasic(City city)
    {
        BasicInfoPanel.SetActive(false);
        CityUiBasic.gameObject.SetActive(true);
        CityUiBasic.City = city;       
    }

    public void ToggleCityUi(City city)
    {
        CityUi.gameObject.SetActive(!CityUi.gameObject.activeSelf);
    }

    public void Deselct()
    {
        BasicInfoPanel.SetActive(true);

        CityUiBasic.gameObject.SetActive(false);
        CityUi.gameObject.SetActive(false);
    }
}
