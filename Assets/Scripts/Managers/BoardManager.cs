﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq; 

public class BoardManager : MonoBehaviour
{
    public Hexagon HexagonPref;
    public City CityPref;

    readonly int _boardWidth = 16;
    readonly int _boardHeight = 12;

    Dictionary<Vector2, Hexagon> _board;
    static BoardManager _instance;


    void Awake()
    {
        _board = new Dictionary<Vector2, Hexagon>();
    }

    public static BoardManager Instance
    {
        get
        {
            if(!_instance)
        {
                var gameObject = GameObject.FindGameObjectWithTag(Tags.BoardManager);
                _instance = gameObject.GetComponent<BoardManager>();
            }

            return _instance;
        }
    }


    public void GenerateBoard()
    {
        CreateBoard();
        FindNeighbours();
        //SpawnPlayerAtRandomHexagon();
        SpawnCityAtRandomHexagon();
    }

    void CreateBoard()
    {
        Transform boardHolder = new GameObject("Board").transform;

        for (var y = 0; y < _boardHeight; y++)
        {
            var translationX = y % 2 == 0 ? 0 : 0.5f;
            var translationY = y * 0.75f;

            for (var x = 0; x < _boardWidth; x++)
                CreateHexagon(boardHolder, x, y, translationX, translationY);
        }
    }

    void CreateHexagon(Transform boardHolder, int x, int y, float translationX, float translationY)
    {
        var position = new Vector2(x + translationX, translationY);
        var hexagon = Instantiate(HexagonPref, position, Quaternion.identity);
        hexagon.Position = position;
        hexagon.transform.SetParent(boardHolder);
        _board.Add(position, hexagon);
    }

    void FindNeighbours()
    {
        foreach (var hexagon in _board)
            FindNeighbours(hexagon);
    }

    void FindNeighbours(KeyValuePair<Vector2, Hexagon> hexagon)
    {
        foreach (var row in new int[] { 1, 2, 3 })
        {
            var neighbours = new List<Hexagon>();

            foreach (var direction in PossibleNeighboursDirections(row))
            {
                var position = hexagon.Key + direction;

                if (_board.ContainsKey(position))                   
                    neighbours.Add(_board[position]);
            }

            hexagon.Value.NeighboursByRow.Add(row, neighbours);
        }         
    }

    List<Vector2> PossibleNeighboursDirections(int row)
    {
        var directions = new List<Vector2>
        {
            new Vector2(-0.5f, 0.75f) * row,
            new Vector2(0.5f, 0.75f) * row,
            new Vector2(0.5f, -0.75f) * row,
            new Vector2(-0.5f, -0.75f) * row,

            new Vector2(-1, 0) * row,
            new Vector2(1, 0) * row    
        };

        if (row.Equals(2))
        {
            directions.Add(new Vector2(0, 1.5f));
            directions.Add(new Vector2(0, -1.5f));

            directions.Add(new Vector2(-1.5f, 0.75f));
            directions.Add(new Vector2(1.5f, 0.75f));
            directions.Add(new Vector2(1.5f, -0.75f));
            directions.Add(new Vector2(-1.5f, -0.75f));
        }

        if (row.Equals(3))
        {
            directions.Add(new Vector2(-0.5f, 2.25f));
            directions.Add(new Vector2(0.5f, -2.25f));
            directions.Add(new Vector2(-0.5f, -2.25f));
            directions.Add(new Vector2(0.5f, 2.25f));

            directions.Add(new Vector2(-2, 1.5f));
            directions.Add(new Vector2(-2, -1.5f));
            directions.Add(new Vector2(2, 1.5f));
            directions.Add(new Vector2(2, -1.5f));

            directions.Add(new Vector2(-2.5f, 0.75f));
            directions.Add(new Vector2(2.5f, 0.75f));
            directions.Add(new Vector2(2.5f, -0.75f));
            directions.Add(new Vector2(-2.5f, -0.75f));
        }

        return directions;
    }

    void SpawnPlayerAtRandomHexagon()
    {    
        var hexagon = _board.ElementAt(Random.Range(0, _board.Count())).Value;
        var player = Character.GetInstacne();
        player.transform.position = hexagon.Position;
        Camera.main.transform.position = new Vector3(hexagon.Position.x, hexagon.Position.y, -1);
        hexagon.ObjectOnHexagon = player;
    }

    void SpawnCityAtRandomHexagon()
    {
        var hexagon = _board.ElementAt(Random.Range(0, _board.Count())).Value;
        var city = Instantiate(CityPref, hexagon.Position, Quaternion.identity);
        hexagon.ObjectOnHexagon = city;
    }

    public Hexagon GetHexagonByPosition(Vector2 position)
    {
        if (_board.ContainsKey(position))
            return _board[position];

        return null;
    }
}
