﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    BoardManager _boardManager;

    void Awake()
    {
        _boardManager = BoardManager.Instance;
    }

    void Start()
    {
        _boardManager.GenerateBoard();
    }
}
