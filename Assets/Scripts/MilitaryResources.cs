﻿using System;

[Serializable]
public class MilitaryResources
{
    public int DefensiveArmy;
    public int OffensiveArmy;
}
