﻿public static class Tags
{
    public static string BoardManager = "BoardManager";
    public static string Player = "Player";
    public static string UiManager = "UiManager";
    public static string Title = "Title";
    public static string Demesne = "Demesne";
    public static string Info = "Info";
    public static string Image = "Image";
    public static string Count = "Count";
}
