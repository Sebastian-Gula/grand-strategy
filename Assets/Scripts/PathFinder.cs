﻿using System.Collections.Generic;
using System.Linq;

public class PathFinder
{
    public List<Hexagon> ShortestPath(Hexagon from, Hexagon destination)
    {
        if (destination.ObjectOnHexagon != null)
            return new List<Hexagon>();

        var end = AStar(from, destination);

        if (end != null)
        {
            var path = new List<Hexagon>();

            while (end.Parent != null)
            {
                path.Add(end.Current);
                end = end.Parent;
            }
            path.Reverse();
            return path;
        }

        return new List<Hexagon>();
    }

    MatrixNode AStar(Hexagon from, Hexagon destination)
    {
        var explored = new Dictionary<string, MatrixNode>();
        var open = new Dictionary<string, MatrixNode>();
        var startNode = new MatrixNode(from, destination);
        var key = from.Position.ToString();
        open.Add(key, startNode);

        while (open.Any())
        {
            var smallestNode = SmallestNode(open);

            foreach(var neighbor in smallestNode.Value.Current.Neighbours)
            {
                if(neighbor.ObjectOnHexagon == null)
                {
                    var current = new MatrixNode(neighbor, destination, smallestNode.Value);
                    if (neighbor == destination)
                        return current;

                    key = neighbor.Position.ToString();
                    if (!open.ContainsKey(key) && !explored.ContainsKey(key))
                        open.Add(key, current);
                }              
            }

            open.Remove(smallestNode.Key);
            explored.Add(smallestNode.Key, smallestNode.Value);
        }

        return null;
    }

    KeyValuePair<string, MatrixNode> SmallestNode(Dictionary<string, MatrixNode> open)
    {
        return open.Aggregate((minItem, nextItem)
            => minItem.Value.CumulativeLength < nextItem.Value.CumulativeLength 
            ? minItem : nextItem);
    }  
}
