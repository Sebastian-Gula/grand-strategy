﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceUi : Ui
{
    Text _resourceCount;

    public string ResourceCount
    {
        set
        {
            _resourceCount.text = value;
        }
    }


    void Awake()
    {
        _resourceCount = FindComponentInChildWithTag<Text>(Tags.Count);
    }
}
