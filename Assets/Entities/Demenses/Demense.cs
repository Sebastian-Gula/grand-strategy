﻿using UnityEngine;

public abstract class Demense : MonoBehaviour, IHexagonAction
{
    public string Name;  

    public abstract void OnMouseDownAction();
    protected abstract void ShowBasicUI();
}
