﻿using System.Collections.Generic;
using UnityEngine.UI;

public class CityUiBasic : Ui
{
    UiManager _uiManager;
    Text _titleText;
    List<Text> _infoTexts;
    City _city;

    public City City
    {
        set
        {
            _city = value;
            _titleText.text = _city.Name;

            for (var i = 0; i < _infoTexts.Count; i++)
                _infoTexts[i].text = _city.Informations[i].ToString();
        }
    }


    void Awake()
    {
        _uiManager = UiManager.Instance;
        _titleText = FindComponentInChildWithTag<Text>(Tags.Title);
        _infoTexts = FindComponentsInChildWithTag<Text>(Tags.Info);
    } 

    public void ToggleUi()
    {
        _uiManager.ToggleCityUi(_city);
    }
}
