﻿using System.Collections.Generic;

public class City : Demense, IHexagonAction
{
    UiManager _uiManager;

    public List<LabelInfo> Informations { get; private set; }

    int _army;
    int _income;
    int _lands;
    int _smth;

    void Awake()
    {
        _uiManager = UiManager.Instance;

        _army = 1032;
        _income = 2;
        _lands = 2;
        _smth = 1;

        Informations = new List<LabelInfo>
        {
            new LabelInfo
            {
                Title = "Army",
                Value = _army.ToString()
            },
            new LabelInfo
            {
                Title = "Income",
                Value = _income.ToString()
            },
            new LabelInfo
            {
                Title = "Lands",
                Value = _lands.ToString()
            },
            new LabelInfo
            {
                Title = "Smth",
                Value = _smth.ToString()
            }
        };
    }


    public override void OnMouseDownAction()
    {
        ShowBasicUI();
    }
    
    protected override void ShowBasicUI()
    {
        _uiManager.ShowCityUiBasic(this);
    }
}
