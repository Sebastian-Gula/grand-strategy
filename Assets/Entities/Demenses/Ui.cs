﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ui : MonoBehaviour
{
    protected T FindComponentInChildWithTag<T>(string tag)
    {
        foreach (Transform tr in transform)
            if (tr.tag == tag)
                return tr.GetComponent<T>();

        return default(T);
    }

    protected List<T> FindComponentsInChildWithTag<T>(string tag)
    {
        var components = new List<T>();

        foreach (Transform tr in transform)
            if (tr.tag == tag)
                components.Add(tr.GetComponent<T>());

        return components;
    }  
}
