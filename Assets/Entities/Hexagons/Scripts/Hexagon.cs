﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BoxCollider2D))]
public class Hexagon : MonoBehaviour
{
    public List<Hexagon> Neighbours = new List<Hexagon>();
    public Dictionary<int, List<Hexagon>> NeighboursByRow = new Dictionary<int, List<Hexagon>>();
    public IHexagonAction ObjectOnHexagon { get; set; }
    public Vector2 Position { get; set; }

    UiManager _uiManager;

    void Awake()
    {
        _uiManager = UiManager.Instance;
    }

    void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (ObjectOnHexagon != null)
                ObjectOnHexagon.OnMouseDownAction();
            else
                _uiManager.Deselct();
        }
       

        /*var player = Character.GetInstacne();
        var boardManager = BoardManager.GetInstacne();
        var from = boardManager.GetHexagonByPosition(player.transform.position);
        var desination = boardManager.GetHexagonByPosition(transform.position);

        if (from && desination)
            player.Action(from, desination);*/
    }
}
