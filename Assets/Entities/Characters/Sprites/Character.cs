﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour, IHexagonAction
{
    private bool IsMoving { get; set; }
    private bool IsMovingOneUnit { get; set; }

    static Character _instance;

    public static Character GetInstacne()
    {
        if(!_instance)
        {
            var gameObject = GameObject.FindGameObjectWithTag(Tags.Player);
            _instance = gameObject.GetComponent<Character>();
        }

        return _instance;
    }


    public void Action(Hexagon from, Hexagon destination)
    {
        if(!IsMoving)
        {
            PathFinder pathFinder = new PathFinder();
            var path = pathFinder.ShortestPath(from, destination);
            from.ObjectOnHexagon = null;
            destination.ObjectOnHexagon = this;
            StartCoroutine(Move(path));
        }     
    }

    IEnumerator Move(List<Hexagon> dest)
    {
        IsMoving = true;
        foreach(var destination in dest)
        {
            StartCoroutine(MoveOneUnit(destination.Position));
            yield return new WaitUntil(() => !IsMovingOneUnit);
        }
        IsMoving = false;
    }

    IEnumerator MoveOneUnit(Vector2 destination)
    {
        IsMovingOneUnit = true;
        float sqrRemainingDistance = (destination - (Vector2)transform.position).sqrMagnitude;

        while (sqrRemainingDistance > float.Epsilon)
        {
            transform.position = Vector2.MoveTowards(transform.position, destination, 1.5f * Time.deltaTime);
            Camera.main.transform.position = transform.position + new Vector3(0, 0, -1);
            sqrRemainingDistance = (destination - (Vector2)transform.position).sqrMagnitude;
            yield return null;
        }
        IsMovingOneUnit = false;
    }

    public void OnMouseDownAction()
    {
        // Oznaczyć jako zaznaczony obiekt (informacje w UI)
        // Wyświetlić UI postaci
        throw new System.NotImplementedException();
    }
}
